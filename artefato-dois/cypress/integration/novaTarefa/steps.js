
Given(/^Acesso a página MyTasks$/, () => {
	cy.get('#my_task').click();
});

When(/^Inserir uma nova tarefa no campo$/, () => {
	cy.get('#new_task').type('nova tarefa');
});

When(/^Clicar no botão adicionar ou apertar Enter$/, () => {
	cy.get('.input-group-addon').click();
});

Then(/^A tarefa deve ser anexada à lista de tarefas criadas$/, () => {
	return true;
});


Given(/^Acesso a página MyTasks$/, () => {
	cy.get('#my_task').click();
});

When(/^Inserir uma nova tarefa no campo com menos que 3 caracteres$/, () => {
	cy.get('#new_task').type('n');
});

When(/^Clicar no botão adicionar ou apertar Enter$/, () => {
	cy.get('.input-group-addon').click();
});

Then(/^A tarefa deve não deve ser anexada a lista de tarefas$/, () => {
	return false;
});

Given(/^Acesso a página MyTasks$/, () => {
	cy.get('#my_task').click();
});

When(/^Inserir uma tarefa com mais que 250 caracteres$/, () => {
	cy.get('#new_task').type('novasdasdasdashjkdhsajhdksjahdjashdkjhaskdjhaskdhjksahdkjhasjkdhasjkhdkjashdjkshahdaskjhdkasjhdkjsahkdjhaskjdhkasjhdkjashdkjhaskjdhaskjhhdsjakhdkjsahkdhkasjhdkjashkjdhjkashdjkhsajkhdjksahdjkhasjkdhaskjhdjkashdkjhasjkdhaskjhdkjashdjkhasjkdhskajhdkjashdjkashjkdhasjkhdkasjhdjkashdjkhsakjdhaskjdhkjas');
});

When(/^Clicar no botão adicionar ou apertar Enter$/, () => {
	cy.get('.input-group-addon').click();
});

Then(/^A tarefa não deve ser anexada a lista de tarefas$/, () => {
	return false;
});
