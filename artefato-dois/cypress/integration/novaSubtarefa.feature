Feature: novaSubtarefa

    Como usuário do aplicativo ToDo, eu deveria ser capaz de criar uma subtarefa
    Para dividir minhas tarefas em partes menores

Scenario: Criação de uma Subtarefa
    Given Acesso a página MyTasks
    When Clico em Manage Subtasks
    And Adiciono uma SubTask Description
    And Adiciono uma Due Date no formato mm dd aaaa
    And Clico no botão Add
    Then Uma nova SubTask deve ser criada

Scenario: Criação de Subtarefa sem descrição e sem data
    Given Acesso a página MyTasks
    When Clico em Manage Subtasks
    And Não adiciono nenhuma desscrição
    And Limpo o campo data
    And Clico no botão Add
    Then Uma nova subtask não deve ser criada
    And Uma mensagem de erro deve informar Preencher os campos

Scenario: Criação de Subtarefa com descrição maior que 250 caracteres
    Given Acesso a pagina MyTasks 
    When Clico em Manage Subtasks
    And Adiciono uma descrição com mais de 250 caracteres
    And Clico no botão Add
    Then Uma nova subtask não deve ser criada 
    And Uma mensagem de erro deve informar Tarefa não deve ter mais que 250 caracteres
