//Criação de uma Subtarefa

Given(/^Acesso a página MyTasks$/, () => {
    cy.get('#my_task').click();
});

When(/^Clico em Manage Subtasks$/, () => {
	cy.get(':nth-child(1) > :nth-child(4) > .btn').click();
});

When(/^Adiciono uma SubTask Description$/, () => {
	cy.get('#new_sub_task').type('Nova SubTask');
});

When(/^Adiciono uma Due Date no formato mm dd aaaa$/, () => {
	cy.get('input[id="dueDate"]').clear();
    cy.get('input[id="dueDate"]').type('05/20/2021');
});

When(/^Clico no botão Add$/, () => {
	cy.get('#add-subtask').click();
});

Then(/^Uma nova SubTask deve ser criada$/, () => {
	return true;
    cy.get('.modal-footer > .btn').click();
});


//Criação de Subtarefa sem descrição e sem data

Given(/^Acesso a página MyTasks$/, () => {
	cy.get('#my_task').click();
});

When(/^Clico em Manage Subtasks$/, () => {
	cy.get(':nth-child(1) > :nth-child(4) > .btn').click();
});

When(/^Não adiciono nenhuma desscrição$/, () => {
	cy.get('#new_sub_task').clear();
});

When(/^Limpo o campo data$/, () => {
	cy.get('input[id="dueDate"]').clear();
});

When(/^Clico no botão Add$/, () => {
	cy.get('#add-subtask').click();
});

Then(/^Uma nova subtask não deve ser criada$/, () => {
	return true;
});

Then(/^Uma mensagem de erro deve informar Preencher os campos$/, () => {
	return false;
});

//Criação de Subtarefa com descrição maior que 250 caracteres

Given(/^Acesso a pagina MyTasks$/, () => {
	cy.get('#my_task').click();
});

When(/^Clico em Manage Subtasks$/, () => {
	cy.get(':nth-child(1) > :nth-child(4) > .btn').click();
});

When(/^Adiciono uma descrição com mais de 250 caracteres$/, () => {
	cy.get('#new_sub_task').type('novasdasdasdashjkdhsajhdksjahdjashdkjhaskdjhaskdhjksahdkjhasjkdhasjkhdkjashdjkshahdaskjhdkasjhdkjsahkdjhaskjdhkasjhdkjashdkjhaskjdhaskjhhdsjakhdkjsahkdhkasjhdkjashkjdhjkashdjkhsajkhdjksahdjkhasjkdhaskjhdjkashdkjhasjkdhaskjhdkjashdjkhasjkdhskajhdkjashdjkashjkdhasjkhdkasjhdjkashdjkhsakjdhaskjdhkjas');
});

When(/^Clico no botão Add$/, () => {
	cy.get('#add-subtask').click();
});

Then(/^Uma nova subtask não deve ser criada$/, () => {
	return true;
});

Then(/^Uma mensagem de erro deve informar Tarefa não deve ter mais que 250 caracteres$/, () => {
	return false;
});
