Feature: novaTarefa

    Como usuário do aplicativo ToDo, eu deveria ser capaz de criar uma tarefa
    Para que eu possa gerenciar minhas tarefas

Scenario: Criação de uma nova tarefa
    Given Acesso a página MyTasks
    When Inserir uma nova tarefa no campo
    And Clicar no botão adicionar ou apertar Enter
    Then A tarefa deve ser anexada à lista de tarefas criadas

Scenario: Criação de uma nova tarefa com menos de de 3 caracteres
    Given Acesso a página MyTasks
    When Inserir uma nova tarefa no campo com menos que 3 caracteres
    And Clicar no botão adicionar ou apertar Enter
    Then A tarefa deve não deve ser anexada a lista de tarefas

Scenario: Criação de uma nova tarefa com mais de 250 caracteres
    Given Acesso a página MyTasks
    When Inserir uma tarefa com mais que 250 caracteres
    And Clicar no botão adicionar ou apertar Enter
    Then A tarefa não deve ser anexada a lista de tarefas
