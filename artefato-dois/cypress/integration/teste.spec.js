context('Como Usuário do Aop ToDo', () => {
    it('Eu deveria ser capaz de criar uma tarefa', () => {
        //rotas
        //POST 201 /tasks.json
        cy.server();
        cy.route('POST','**/tasks.json**' ).as('postNewTask');
        cy.get('#my_task').click();
        cy.get('#new_task').type('nova tarefa');
        cy.get('.input-group-addon').click();

        cy.wait('@postNewTask').then((resNewTask) => {
            console.log(resNewTask.responseBody)
            cy.log(resNewTask.responseBody)
        });
    });
});

