## Melhorias sugeridas para o App

1. Subtarefas poderiam ser gerenciadas em um Menu ao lado do menu para ver o dashboard de tarefas, deixando a página de ToDo mais minimalista e intuitiva.
2. Subtarefas poderiam ser exibidas em uma espécie de menu toggle sendo exibidas quando clicamos na tarefa principal e exibindo seus próprios checkbox para que possamos acompanhar em que subtarefa estamos de forma mais simples.
3. Ao usar o checkbox de Done o ToDo poderia ser movido para o final da fila.
4. Fazer o projeto responsivo, para que possa ser mais facilmente vizualizado em dispositivos móveis.






 
